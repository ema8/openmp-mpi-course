import sys

filename = sys.argv[1]

initGrid = "InitGrid"
initGridVal = 0
genField = "GenerationField"
genFieldVal = 0
partGen = "ParticleGeneration"
partGenVal = 0
sysEv = "SystemEvolution"
sysEvVal = 0

step = 3

f = open(filename, "r")
for i in f.readlines():
	tok = i.split(" ")
	if initGrid == tok[0]:
		initGridVal += float(tok[-2])
	elif genField == tok[0]:
		genFieldVal += float(tok[-2])
	elif partGen == tok[0]:
		partGenVal += float(tok[-2])
	elif sysEv == tok[0]:
		sysEvVal += float(tok[-2])

print(initGridVal/step)
print(genFieldVal/step)
print(partGenVal/step)
print(sysEvVal/step)
