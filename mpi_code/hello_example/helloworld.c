/* Example: Hello
 *
 * All processes print an Hello World with its rank
*/  
#include <stdio.h>
#include "mpi.h"
#include "omp.h"

int main(int argc, char **argv)
{
    int rank, size;
    
    /* initialize MPI */
    MPI_Init(&argc, &argv);

    /* get my identifier in standard communicator */
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    /* get the number of processes in standard communicator */
    MPI_Comm_size(MPI_COMM_WORLD, &size);


#pragma omp parallel
{
	printf("num threads = %d\n", omp_get_num_threads());
    printf("Hello! I am thread %d at process %d of %d\n", omp_get_thread_num(), rank, size);
 }    
    /* finalize MPI */
    MPI_Finalize();

    return 0;
}

